use blog;
db.posts.aggregate([
    {"$unwind":"$comments"},
    /* now group by comments, counting each comment */
    {"$group":
    {"_id":"$comments.author",
        "count":{$sum:1}
    }
    },
    /* sort by popularity */
    {"$sort":{"count":-1}},
    /* show me the top 10 */
    {"$limit": 10}

]);