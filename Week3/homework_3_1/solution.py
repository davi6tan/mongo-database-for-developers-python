__author__ = 'davidtan'

import decimal
import pymongo
import sys

# connnecto to the db on standard port
connection = pymongo.MongoClient("mongodb://localhost")

db = connection.schoolPy  # attach to db
collection = db.students  # specify the colllection

try:
    minscore = decimal.Decimal(-1)
    min_i = 0
    i = 0
    iter = collection.find({});
    for item in iter:
        minscore = 101
        i = 0
        for value in item['scores']:
            if value['type'] == "homework":
                score = decimal.Decimal(value['score'])
                if minscore == decimal.Decimal(-1) or score < minscore:
                    minscore = score
                    min_i = i
            i += 1
        item['scores'].pop(min_i)
        collection.save(item)

except:
    print "Error trying to read collection:" + sys.exc_info()[0]